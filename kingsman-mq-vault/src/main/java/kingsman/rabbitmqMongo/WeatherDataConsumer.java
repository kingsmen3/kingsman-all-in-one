package kingsman.rabbitmqMongo;

import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.pubsub.v1.AckReplyConsumer;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.pubsub.v1.PubsubMessage;
import com.google.pubsub.v1.SubscriptionName;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import io.grpc.Context;
import org.bson.Document;
import org.json.JSONObject;


import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;

public class WeatherDataConsumer {

    private static final String projectId = "local-tempo-407113";
    private static final String credentialsPath = "secrets.json";
    private static final String credentialsPathGcp = "secretsGcp.json";

    private static final String gcpBucketName = "kingsman-weather-watcher";

    private static void startSubscriber(String subscriptionId, String mongoCollectionName) throws IOException {

        GoogleCredentials credentials = GoogleCredentials.fromStream(WeatherDataConsumer.class.getClassLoader().getResourceAsStream(credentialsPath))
                .createScoped(Collections.singletonList("https://www.googleapis.com/auth/cloud-platform"));
        GoogleCredentials credentialsGcs = GoogleCredentials.fromStream(WeatherDataConsumer.class.getClassLoader().getResourceAsStream(credentialsPathGcp))
                .createScoped(Collections.singletonList("https://www.googleapis.com/auth/cloud-platform"));
        SubscriptionName subscriptionName = SubscriptionName.of(projectId, subscriptionId);
        /*
        MongoClient mongoClient = MongoClients.create(mongoDbUri);
        MongoDatabase mongoDatabase = mongoClient.getDatabase(mongoDatabaseName);
        MongoCollection<Document> collection = mongoDatabase.getCollection(mongoCollectionName);
        */




        MessageReceiver receiver = (PubsubMessage message, AckReplyConsumer consumer) -> {
            System.out.println("Id: " + message.getMessageId());
            System.out.println("Data: " + message.getData().toStringUtf8());
            String jsonData = message.getData().toStringUtf8();

            try {

                Storage storage = StorageOptions.newBuilder()
                        .setCredentials(credentialsGcs)
                        .build()
                        .getService();

                BlobInfo blobInfo = BlobInfo.newBuilder(gcpBucketName, "weather-data/" + System.currentTimeMillis() + ".json")
                        .build();
                Blob blob = storage.create(blobInfo, jsonData.getBytes(StandardCharsets.UTF_8));

                // Acknowledge the message after successfully uploading to GCS
                consumer.ack();
            } catch (Exception e) {
                // Handle error, e.g., logging
                System.err.println("Error handling message: " + e.getMessage());
            }


        };

        Subscriber subscriber = Subscriber.newBuilder(String.valueOf(subscriptionName), receiver)
                .setCredentialsProvider(FixedCredentialsProvider.create(credentials)).build();

        subscriber.startAsync().awaitRunning();
    }


    public static void main(String... args) {
        try {
            startSubscriber("commands-sub", "commands");
            startSubscriber("weather-data-queue-sub", "weather_data");
            startSubscriber("results-real-time-q-sub", "results");
            startSubscriber("automatic-commands-sub", "automatic_commands");

            Thread.sleep(Long.MAX_VALUE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}