package kingsman.real;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.core.ApiFuture;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.pubsub.v1.AckReplyConsumer;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.ProjectSubscriptionName;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.PubsubMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Component
public class MessageReceiver {

    private List<JsonNode> messageBuffer = new ArrayList<>();
    private static final int BATCH_SIZE = 300;
    private RedisTemplate<String, String> redisTemplate;

    private static final String projectId = "local-tempo-407113";
    private static final String credentialsPath = "src/main/resources/secrets.json";
    GoogleCredentials credentials = GoogleCredentials.fromStream(new FileInputStream(credentialsPath))
            .createScoped(Collections.singletonList("https://www.googleapis.com/auth/cloud-platform"));

    private static Publisher publisher = null;



    @Autowired
    public MessageReceiver(RedisTemplate<String, String> redisTemplate) throws IOException {
        this.redisTemplate = redisTemplate;
        createPubSubSubscriber();
    }

    private void createPubSubSubscriber() {
        String subscriptionId = "weather-data-queue-real-time-sub";
        ProjectSubscriptionName subscriptionName = ProjectSubscriptionName.of(projectId, subscriptionId);
        Subscriber subscriber = Subscriber.newBuilder(subscriptionName, this::receiveMessage)
                .setCredentialsProvider(FixedCredentialsProvider.create(credentials))
                .build();
        subscriber.startAsync().awaitRunning();
    }

    public void receiveMessage(PubsubMessage message, AckReplyConsumer consumer) {
        String receivedMessage = message.getData().toStringUtf8();
        // Process the incoming message and store it in Redis

        processAndStore(receivedMessage);
        System.out.println("Received message: " + receivedMessage);

        JsonNode jsonNode = convertStringToJsonNode(receivedMessage);
        messageBuffer.add(jsonNode);
        if (messageBuffer.size() >= BATCH_SIZE) {
            String result = performCalculations();
            sendResultToPubSub(result);
            messageBuffer.clear();
        }

        consumer.ack(); // Acknowledge the message after processing
    }

    private void sendResultToPubSub(String result) {
        //String rabbitMQHost = "192.168.10.22";
        //String rabbitMQHost = "172.31.252.134";
        //String rabbitMQQueue = "results-real-time-q";
        //int rabbitPort = 5672;
        //int rabbitPort = 1901;

        try {
            ProjectTopicName topicName = ProjectTopicName.of(projectId, "results-real-time-q");
            publisher = Publisher.newBuilder(topicName).setCredentialsProvider(FixedCredentialsProvider.create(credentials)).build();

            publishToPubSub(result,publisher);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private void processAndStore(String message) {

        redisTemplate.opsForValue().set("real-time-weather", message);
        System.out.println("Processed and stored message: " + message);
    }
    public String performCalculations() {
        double avgTimestamp = 0;
        double avgTemperature = 0;
        double avgPressure = 0;
        double avgHumidity = 0;
        double avgPrecipitation = 0;
        double avgWindSpeed = 0;
        double avgWindDirection = 0;

        for (JsonNode jsonNode : messageBuffer) {
            avgTimestamp += jsonNode.get("timestamp").asDouble();
            avgTemperature += jsonNode.get("temperature").asDouble();
            avgPressure += jsonNode.get("pressure_pa").asDouble();
            avgHumidity += jsonNode.get("humidity_%").asDouble();
            avgPrecipitation += jsonNode.get("precipitation_mm").asDouble();
            avgWindSpeed += jsonNode.get("wind_speed_mps").asDouble();
            avgWindDirection += jsonNode.get("wind_direction_deg").asDouble();
        }

        int bufferSize = messageBuffer.size();
        avgTimestamp /= bufferSize;
        avgTemperature /= bufferSize;
        avgPressure /= bufferSize;
        avgHumidity /= bufferSize;
        avgPrecipitation /= bufferSize;
        avgWindSpeed /= bufferSize;
        avgWindDirection /= bufferSize;

        // Create a JSON object for the average values
        String avgResult = String.format("{\"avg_timestamp\": %f, \"avg_temperature\": %f, " +
                        "\"avg_pressure\": %f, \"avg_humidity\": %f, \"avg_precipitation\": %f, " +
                        "\"avg_wind_speed\": %f, \"avg_wind_direction\": %f}", avgTimestamp, avgTemperature,
                avgPressure, avgHumidity, avgPrecipitation, avgWindSpeed, avgWindDirection);

        return avgResult;
    }
    private JsonNode convertStringToJsonNode(String jsonString) {
        try {
            return new ObjectMapper().readTree(jsonString);
        } catch (IOException e) {
            throw new RuntimeException("Error converting JSON string to JsonNode", e);
        }
    }

    private static void publishToPubSub(String message, Publisher publisher) {
        ByteString data = ByteString.copyFromUtf8(message);
        PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();

        try {
            // Once published, returns a server-assigned message ID (unique within the topic)
            ApiFuture<String> messageIdFuture = publisher.publish(pubsubMessage);
            String messageId = messageIdFuture.get();
            System.out.println("Published message ID: " + messageId);
            System.out.println("Sent to queue "+publisher.getTopicNameString()+" : " + message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}